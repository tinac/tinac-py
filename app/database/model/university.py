# Copyright (C) 2018  TiNAC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .base import Base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship

class University(Base):
    __tablename__ = "universities"

    ref = Column(Integer)
    name = Column(String(100))
    headquarters = Column(String(100))
    region = Column(String(100))
    established = Column(Integer)
    type = Column(String(50))

    schools = relationship("School")

    def __init__(self,ref, name, headquarters, region, established, type, schools):
        self.ref = ref
        self.name = name
        self.headquarters = headquarters
        self.region = region
        self.established = established
        self.type = type
        self.schools = schools