# Copyright (C) 2018  TiNAC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .base import Base

from sqlalchemy import Table, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship

degrees_subjects_association = Table('degrees_subjects', Base.metadata,
                            Column('degrees.id', Integer, ForeignKey('degrees.id')),
                            Column('subjects.id', Integer, ForeignKey('subjects.id'))
)

class Degree(Base):
    __tablename__ = "degrees"

    ref = Column(Integer)
    name = Column(String(100))
    code = Column(String(100))

    school = Column(Integer, ForeignKey("schools.id"))
    subjects = relationship("Subject", secondary=degrees_subjects_association)
