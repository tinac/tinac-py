# Copyright (C) 2018  TiNAC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import as_declarative, declared_attr, declarative_base

# Define a base model for other database tables to inherit
class BaseModel(object):
    # @declared_attr
    # def __tablename__(cls):
    #     return cls.__name__.lower()

    id = Column(Integer, primary_key=True)
    date_created = Column(DateTime,  default=datetime.datetime.now)
    date_modified = Column(DateTime,  default=datetime.datetime.now, onupdate=datetime.datetime.now)

    def as_dict(self):
        tmp = self.__dict__
        tmp.pop('_sa_instance_state', None)
        return tmp

Base = declarative_base(cls=BaseModel)