# Copyright (C) 2018  TiNAC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .base import Base
from sqlalchemy import Column, Integer, String


class Subject(Base):
    __tablename__ = "subjects"

    ref = Column(Integer)
    name = Column(String(100))
    code = Column(String(100))
    course = Column(String(100))
    type = Column(String(100))
    credits = Column(Integer)
    period = Column(String(100))
    availability = Column(String(100))
    limit = Column(Integer)
    language = Column(String(100))
