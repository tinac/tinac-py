# Copyright (C) 2018  TiNAC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app import database_config
from . import model


engine = None
session = None

def create_connection():
    global engine
    # engine = create_engine('sqlite:///:memory:', echo=True)
    print(f"Engine created: {database_config['uri']}")
    engine = create_engine(str(database_config['uri']), echo=True)    
    
def connect():
    global session
    if session is None:
        Session = sessionmaker(bind=engine)
        session = Session()        

def close():
    global session
    session.close()
    session = None

def initial_migration():        
    model.Base.metadata.bind = engine
    model.Base.metadata.create_all(engine)

def create_university(university):
    session.add(university)
    session.commit()
