# Copyright (C) 2018  TiNAC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

database_config = {}
database_config['storage'] = 'database.sqlite3'

uri = os.path.join(os.environ['BASE_PATH'], database_config['storage'])
database_config['uri'] = f"sqlite:///{uri}"

# 'sqlite:///' + os.path.join(os.environ['BASE_PATH'], database_config['storage'])

# engine = create_engine('sqlite:///:memory:', echo=True)
# engine = create_engine(db_uri, echo=True)
# Session = sessionmaker(bind=engine)

from . import database

def run():    
    database.create_connection()
    database.connect()
    database.initial_migration()    
    universidad = database.model.university.University(1, "Unizar", "Zaragoza", "Aragon", 1999, "publica", [])    
    database.create_university(universidad)
    